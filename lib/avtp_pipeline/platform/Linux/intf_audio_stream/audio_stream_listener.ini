#####################################################################
# General Listener configuration
#####################################################################
role = listener

# stream_addr & stream_uid: Combines into a single unique stream ID.
# The talker and listener must both have this set the same.
stream_addr = E8:E0:B7:B5:7D:F8
stream_uid = 1

# dest_addr: When SRP is being used the destination address only needs to
# be set in the talker.  If SRP is not being used the destination address
# needs to be set in both side the talker and listener.
# The destination is a multicast address, not a real MAC address, so it
# does not match the talker or listener's interface MAC.  There are 
# several pools of those addresses for use by AVTP defined in 1722.
# At this time they need to be locally administered and must be in the range
# of 91:E0:F0:00:FE:00 - 91:E0:F0:00:FE:FF.
# Typically :00 for the first stream, :01 for the second, etc.
dest_addr = 91:e0:f0:00:fe:00

# max_transit_usec: Allows manually specifying a maximum transit time. 
# On the talker this value is added to the PTP walltime to create the AVTP Timestamp.
# On the listener this value is used to validate an expected valid timestamp range.
# Note: For the listener the map_nv_item_count value must be set large enough to 
# allow buffering at least as many AVTP packets that can be transmitted  during this 
# max transit time.
max_transit_usec = 50000

# max_stale: The number of microseconds beyond the presentation time that media queue items will be purged 
# because they are too old (past the presentation time). This is only used on listener end stations.
# Note: needing to purge old media queue items is often a sign of some other problem. For example: a delay at 
# stream startup before incoming packets are ready to be processed by the media sink. If this deficit 
# in processing or purging the old (stale) packets is not handled, syncing multiple listeners will be problematic.
#max_stale = 1000

# raw_rx_buffers: The number of raw socket receive buffers. Typically 50 - 100 are good values.
# This is only used by the listener. If not set internal defaults are used.
#raw_rx_buffers = 100

# report_seconds: How often to output stats. Defaults to 10 seconds. 0 turns off the stats. 
report_seconds = 5

#####################################################################
# Mapping module configuration
#####################################################################
map_lib = libopenavb_map_aaf_audio.so

# map_fn: The name of the initialize function in the mapper.
map_fn = openavbMapAVTPAudioInitialize

# map_nv_item_count: The number of media queue elements to hold.
map_nv_item_count = 128

# map_nv_tx_rate: Transmit rate.
# This must be set for the AAF audio mapping module.
map_nv_tx_rate = 1000

# map_nv_packing_factor: Multiple of how many packets of audio frames to place in a media queue item.
map_nv_packing_factor = 8

# map_nv_sparse_mode: if set to 0 presentation time should be
# valid in each packet. Set to 1 to use sparse mode - presentation
# time should be valid in every 8th packet.
#map_nv_sparse_mode = 1

#####################################################################
# Interface module configuration
#####################################################################
intf_lib = libopenavb_intf_audio_stream.so

# intf_fn: The name of the initialize function in the interface.
intf_fn = openavbIntfAudioStreamInitialize

# intf_nv_ignore_timestamp: If set the listener will ignore the timestamp on media queue items.
# intf_nv_ignore_timestamp = 1

# intf_nv_audio_rate: Valid values that are supported by AAF are:
#  8000, 16000, 32000, 44100, 48000, 88200, 96000, 176400 and 192000
intf_nv_audio_rate = 48000

# intf_nv_audio_bit_depth: Valid values that are supported by AAF are:
#  16, 32
intf_nv_audio_bit_depth = 16

# intf_nv_audio_channels: Valid values that are supported by AAF are:
#  1 - 8
intf_nv_audio_channels = 2

# intf_nv_socket_path: Path to the socket that will be used to send data to the eAVB HAL.
# Note: This needs to match with the value set in the audio policy configuration
# ex:  <devicePort tagName="eAVB Out"
#                  type="AUDIO_DEVICE_IN_BUILTIN_MIC"
#                  role="source"
#                  address="skt_name=eavb_in_1">
intf_nv_socket_name = eavb_in_1
